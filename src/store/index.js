import Vue from "vue";
import Vuex from "vuex";

import i18n from "../plugins/i18n";
import vuetify from "../plugins/vuetify";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    successNotification: false,
    errorNotification: false,
    notificationMsg: "",
    loginModal: false,
    loggedUsername: localStorage.getItem("username"),
    loggedUserId: localStorage.getItem("userId"),
    loggedUserJwt: localStorage.getItem("userJwt"),
    adSelectedCategory: "all"
  },
  mutations: {
    openLoginModal(state) {
      state.loginModal = true;
    },
    setAdSelectedCategory(state, { category }) {
      state.adSelectedCategory = category;
    },
    closeLoginModal(state) {
      state.loginModal = false;
    },
    notificate(state, { type, msg }) {
      if (type === "error") {
        state.errorNotification = true;
      } else {
        state.successNotification = true;
      }
      state.notificationMsg = msg;
    },
    closeNotification(state) {
      state.errorNotification = false;
      state.successNotification = false;
    },
    logout(state) {
      state.loggedUsername = "";
      state.loggedUserId = "";
      state.loggedUserJwt = "";
      localStorage.removeItem("username");
      localStorage.removeItem("userId");
      localStorage.removeItem("userJwt");
    },
    login(state, { loggedUsername, loggedUserId, loggedUserJwt }) {
      state.loggedUsername = loggedUsername;
      state.loggedUserId = loggedUserId;
      state.loggedUserJwt = loggedUserJwt;
      localStorage.setItem("username", loggedUsername);
      localStorage.setItem("userId", loggedUserId);
      localStorage.setItem("userJwt", loggedUserJwt);
    }
  },
  actions: {},
  getters: {
    textDirection() {
      return i18n.locale === "ar" ? "rtl" : "auto";
    },
    isUserLogged(state) {
      return Boolean(state.loggedUserId);
    },
    isMobile() {
      return vuetify.framework.breakpoint.xs;
    }
  }
});
