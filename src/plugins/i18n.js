import Vue from "vue";
import VueI18n from "vue-i18n";

import arMessages from "../intl/ar";
import enMessages from "../intl/en";
import frMessages from "../intl/fr";

Vue.use(VueI18n);

const i18n = new VueI18n({
  locale: navigator.language || navigator.userLanguage,
  messages: {
    ...arMessages,
    ...enMessages,
    ...frMessages
  }
});

export default i18n;
