import App from "./App.vue";
import Vue from "vue";

import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import i18n from "./plugins/i18n";
import apolloProvider from "./plugins/apollo";

import "./registerServiceWorker";

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  apolloProvider,
  i18n,
  render: h => h(App)
}).$mount("#app");
