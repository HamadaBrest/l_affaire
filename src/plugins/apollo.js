import ApolloClient from "apollo-client";
import Vue from "vue";
import VueApollo from "vue-apollo";
import { InMemoryCache } from "apollo-cache-inmemory";
import { createUploadLink } from "apollo-upload-client";

import { graphQLUri } from "../../config.json";

Vue.use(VueApollo);

const getHeaders = () => {
  const headers = {};
  const token = window.localStorage.getItem("userJwt");
  if (token) {
    headers.authorization = `Bearer ${token}`;
  }
  return headers;
};

const uploadLink = createUploadLink({
  uri: graphQLUri,
  headers: getHeaders()
});

const client = new ApolloClient({
  link: uploadLink,
  cache: new InMemoryCache({
    addTypename: true
  })
});

const apolloProvider = new VueApollo({
  defaultClient: client
});

export default apolloProvider;
