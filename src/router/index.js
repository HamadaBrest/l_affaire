import Vue from "vue";
import VueRouter from "vue-router";
import Home from "../views/Home.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/addAdvertisement",
    name: "AddAdvertisement",
    component: () => import("../views/AddAdvertisement.vue")
  },
  {
    path: "/Advertisement/:id",
    name: "Advertisement",
    component: () => import("../views/Advertisement.vue")
  },
  {
    path: "/createAccount",
    name: "CreateAccount",
    component: () => import("../views/CreateAccount.vue")
  },
  {
    path: "/myAdvertisements",
    name: "MyAdvertisements",
    component: () => import("../views/MyAdvertisements.vue")
  },
  {
    path: "/about",
    name: "About",
    component: () => import("../views/About.vue")
  }
];

const router = new VueRouter({
  routes
});

export default router;
